module gitlab.com/pet-pr-social-network/api-gateway

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.29.1
	github.com/stretchr/testify v1.8.4
	github.com/swaggo/http-swagger/v2 v2.0.1
	github.com/swaggo/swag v1.16.1
	gitlab.com/pet-pr-social-network/feed-service v0.0.0-20230606171605-cbe0a59020c4
	gitlab.com/pet-pr-social-network/post-service v0.0.0-20230605170712-906e3ae055a8
	gitlab.com/pet-pr-social-network/relation-service v0.0.0-20230605154209-a4ba2324e44e
	gitlab.com/pet-pr-social-network/user-service v0.0.0-20230605151201-94a39bcf8372
	google.golang.org/grpc v1.55.0
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/spec v0.20.9 // indirect
	github.com/go-openapi/swag v0.22.3 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	github.com/swaggo/files/v2 v2.0.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	golang.org/x/tools v0.9.3 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230530153820-e85fd2cbaebc // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
