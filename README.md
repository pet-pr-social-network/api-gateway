# Implementation of the primitive social network

## For what?
This is a learning project for practice with:
* Microservice architecture
* gRPC
* REST
* Different SQL/NOSQL databases
* Configuring Nginx
* Configuring docker-compose
* TODO

## Start service
`make start`

http://localhost/swagger/index.html

## Stop service
`make stop`

## Repositories of all services
* Gitlab group: https://gitlab.com/pet-pr-social-network
  * api-gateway: https://gitlab.com/pet-pr-social-network/api-gateway
  * user-service: https://gitlab.com/pet-pr-social-network/user-service
  * post-service: https://gitlab.com/pet-pr-social-network/post-service
  * relation-service: https://gitlab.com/pet-pr-social-network/relation-service
